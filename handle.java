
import java.util.ArrayList;
import java.util.List;

public class handle {
    public static void main(String[] args) {
        List<codeItem> listCode = new ArrayList<>();
        listCode.add(new codeItem(1, 200));
        listCode.add(new codeItem(2, 100));
        listCode.add(new codeItem(1, 100));
        listCode.add(new codeItem(2, 200));


        List<productItem> listProduct = new ArrayList<>();
        listProduct.add(new productItem(1, 100, "11/02/2024"));
        listProduct.add(new productItem(2, 400, "01/04/2024"));
        listProduct.add(new productItem(1, 50, "10/02/2024"));
        listProduct.add(new productItem(1, 170, "09/02/2024"));
        listProduct.add(new productItem(3, 200, "09/02/2024"));
        listProduct.add(new productItem(2, 500, "12/05/2024"));
        System.out.println(handle(listCode, listProduct));
    }

    public static result handle(List<codeItem> listCode, List<productItem> listProduct) {
        //Lấy ra các mã code xuất hiện trong giỏ hàng
        List<Integer> maCode = new ArrayList<>();
        for (int i = 0; i < listCode.size(); i++) {
            if (listCode.get(i).getCode() >= 0) {
                maCode.add(listCode.get(i).getCode());
            }
        }
        //Lọc ra và tạo ra 1 mảng chứa các mã code không trùng lặp
        List<Integer> listmaCode = maCode.stream().distinct().toList();

        // tính tổng số number của các mã code có trong giỏ hàng
        List<codeItem> tongMaCode = new ArrayList<>();
        for (int j = 0; j < listmaCode.size(); j++) {
            tongMaCode.add(new codeItem(listmaCode.get(j), 0));
        }
        for (int k = 0; k < tongMaCode.size(); k++) {
            for (int i = 0; i < listCode.size(); i++) {
                if (tongMaCode.get(k).getCode() == listCode.get(i).getCode()) {
                    Integer tongMoi = tongMaCode.get(k).getNumbers() + listCode.get(i).getNumbers();
                    tongMaCode.get(k).setNumbers(tongMoi);
                }
            }
        }
//        System.out.println("tổng số lượng của 1 mã sp trong giỏ hàng:" +" "+tongMaCode);

        // tính tổng số number của các mã code trong kho hàng
        List<Integer> maProduct = new ArrayList<>();
        for (int i = 0; i < listCode.size(); i++) {
            if (listCode.get(i).getCode() >= 0) {
                maProduct.add(listCode.get(i).getCode());
            }
        }
        //Lọc ra và tạo ra 1 mảng chứa các mã code không trùng lặp trong kho hàng
        List<Integer> listmaProduct = maProduct.stream().distinct().toList();

        // tính tổng số number của các mã code có trong kho hàng
        List<codeItem> tongMaProduct = new ArrayList<>();
        for (int j = 0; j < listmaCode.size(); j++) {
            tongMaProduct.add(new codeItem(listmaProduct.get(j), 0));
        }
        for (int k = 0; k < tongMaProduct.size(); k++) {
            for (int i = 0; i < listProduct.size(); i++) {
                if (tongMaProduct.get(k).getCode() == listProduct.get(i).getCode()) {
                    Integer tongMoi = tongMaProduct.get(k).getNumbers() + listProduct.get(i).getNumbers();
                    tongMaProduct.get(k).setNumbers(tongMoi);
                }
            }
        }
        System.out.println(tongMaCode);
        System.out.println(tongMaProduct);

        // kiểm tra xem số lượng number trong giỏ hàng có lớn hơn trong kho hàng không
        for (int j = 0; j < tongMaCode.size(); j++) {
            for(int x = 0; x < tongMaProduct.size(); x++){
                //Kiểm tra các mã code được gọi ra từ mảng tổng số lượng sản phẩm của 1 mã code có giống nhau
                if(tongMaCode.get(j).getCode() == tongMaProduct.get(x).getCode()){
                    //nếu đúng, thì kiểm tra xem số lượng sản phẩm có thỏa mãn không (tổng sp/mã code trong giỏ hàng <= tổng sp/mã code trong kho hàng)
                    if(tongMaCode.get(j).getNumbers() <= tongMaProduct.get(x).getNumbers()){
                        //nếu đúng, thì tạo lọc ra thời gian trong kho hàng của mã code của phần tử được gọi ra của mảng tổng số lượng của 1 mã trong giỏ hàng
                        List<String> times = new ArrayList<>();
                        for (int i = 0; i < listProduct.size(); i++) {
                            // sau khi kiểm tra tổng số lượng sp trong giỏ hàng thỏa mãn số lượng
                            if (tongMaCode.get(j).getCode() == listProduct.get(i).getCode()) {
                                times.add(listProduct.get(i).getTime());
                            }
                        }
                        //lấy được mảng thời gian, sẽ sắp xếp theo thứ tự thời gian gần nhất -> xa nhất
                        List<String> timeSort = times.stream().sorted().toList();
                        System.out.println(timeSort);
                        //trừ số lượng mã code sp trong kho hàng với số lượng của giỏ hàng tuơng ứng
                        // gọi ra lần lượt các tổng số lượng của các mã code trong kho hàng
                                for (int m = 0; m < timeSort.size(); m++) {
                                    for (int n = 0; n < listProduct.size(); n++) {
                                        //kiểm tra thời gian được lấy ra từ mảng chứa thời gian đã được sắp xếp của mã code tồn tại ở mã code có time giống nó,
                                        // và kiểm tra tổng kiểm tra xem mã code trong mảng kho hàng có đúng với mã code của giỏ hàng không
                                        if (listProduct.get(n).getTime() == timeSort.get(m) && listProduct.get(n).getCode() == tongMaCode.get(j).getCode() ) {
                                            //nếu đúng, thực hiện trừ và cập nhật lại số lượng của mã code trong kho hàng với thời gian khác nhau và tổng số lượng của mã code trong giỏ hàng
                                            if (listProduct.get(n).getNumbers() > tongMaCode.get(j).getNumbers()) {
                                                Integer number2 = listProduct.get(n).getNumbers() - tongMaCode.get(j).getNumbers();
                                                listProduct.get(n).setNumbers(number2);
                                                tongMaCode.get(j).setNumbers(0);
                                            } else {
                                                Integer number2 = tongMaCode.get(j).getNumbers() - listProduct.get(n).getNumbers();
                                                listProduct.get(n).setNumbers(0);
                                                tongMaCode.get(j).setNumbers(number2);
                                            }
                                        }
                                    }

                        }
                    }
                    //nếu tổng số lượng của mã code trong giỏ hàng lớn hơn tổng số lượng mã code trong kho thì sẽ ko cho người dùng thực hiện nữa và trả ra thông báo lỗi
                    else{
                        return new result(false,"Trừ không thành công do có mã sản phẩm vượt quá số lượng tồn trong kho", null);
                    }
                }
            }
        }
        //sau khu thực hiện xong sẽ trả ra kết quả cuối cùng
        return new result(true,"đã trừ số lượng sản phẩm theo mã code thành công", listProduct.toString());
    }
}
