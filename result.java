import java.util.List;

public class result {
    private Boolean status;
    private String message;
    private String content;

    public result(Boolean status, String message, String content) {
        this.status = status;
        this.message = message;
        this.content = content;
    }

    @Override
    public String toString() {
        return "result{" +
                "status=" + status + "," +  "\n" +
                "message='" + message  + "," + '\n'+
                "content=" + content +
                '}';
    }
}
