import java.util.Date;

          public class codeItem{
              private Integer code;
              private Integer numbers;

              public codeItem(Integer code, Integer numbers) {
                  this.code = code;
                  this.numbers = numbers;
              }

              public Integer getCode() {
                  return code;
              }

              public void setCode(Integer code) {
                  this.code = code;
              }

              public Integer getNumbers() {
                  return numbers;
              }

              public void setNumbers(Integer numbers) {
                  this.numbers = numbers;
              }

              @Override
              public String toString() {
                  return "codeItem{" +
                          "code=" + code +
                          ", numbers=" + numbers +
                          '}';
              }
          }
