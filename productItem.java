import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;



public class productItem {
    private Integer code;
    private Integer numbers;
    private String time;

    public productItem(Integer code, Integer numbers, String time) {
        this.code = code;
        this.numbers = numbers;
        this.time=time;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getNumbers() {
        return numbers;
    }

    public void setNumbers(Integer numbers) {
        this.numbers = numbers;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "productItem{" +
                "code=" + code +
                ", numbers=" + numbers +
                ", time=" + time +
                '}';
    }
}
